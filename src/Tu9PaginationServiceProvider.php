<?php
// MyVendor\contactform\src\TU9ServiceProvider.php
namespace Tu9\Tu9Pagination;

use Illuminate\Support\Facades\Blade;
use Illuminate\Support\ServiceProvider;
use Tu9\Tu9Pagination\View\Components\Tu9pagination;

class Tu9PaginationServiceProvider extends ServiceProvider
{
    public function boot()
    {
        $this->loadViewsFrom(__DIR__ . '/../resources/views', 'tu9-pagination');
        Blade::component('tu9-pagination', Tu9pagination::class);
    }

    public function register()
    {
    }
}
